
//Slide 4
public class MachineBuilder {
    String name;
    MachineType type;
    Size size;

    public MachineBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public MachineBuilder withType (MachineType type) {
        this.type = type;
        return this;
    }

    public MachineBuilder withSize (Size size) {
        this.size = size;
        return this;
    }

    public Machine build() {
        Machine m = new Machine();
        m.setName(this.name);
        m.setType(this.type);
        m.setSize(this.size);
        return m;
    }
}
