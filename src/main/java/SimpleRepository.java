import java.util.List;
import java.util.stream.Collectors;

//Slide 7
public class SimpleRepository {

    private SimpleStorage storage;

    //konstruktor
    public SimpleRepository(SimpleStorage storage) {
        this.storage = storage;
    }

    void add(Machine machine) {
        storage.add(machine);
    }

    boolean remove(Machine machine) {
            if(storage.getAll() == null) {
            return false;
        }
        storage.remove(machine);
        return true;
    }

    List<Machine> find(MachineType machineType) {
        return storage.getAll().stream()
                .filter(machine -> machine.getType()==machineType)
                .collect(Collectors.toList());
    }

}
