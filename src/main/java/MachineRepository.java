import java.util.List;

// Slajd 8
public interface MachineRepository {

    void add(Machine machine);

    boolean remove(Machine machine);

    List<Machine> getAll();
    List<Machine> findByType(MachineType machineType);
    List<Machine> findByName(String machineName);




}
