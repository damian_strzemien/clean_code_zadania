import java.util.ArrayList;
import java.util.List;

//Slajd 6
public class SimpleStorage implements Storage {
    private List<Machine> dataStorage = new ArrayList<>();

    @Override
    public void add(Machine machine) {
                if(machine == null) {
            throw new RuntimeException();
        }
        dataStorage.add(machine);
    }

    @Override
    public boolean remove(Machine machine) {
        dataStorage.remove(machine);
        return false;
    }

    @Override
    public List<Machine> getAll() {
        List<Machine> machines = new ArrayList<>();
        machines.addAll(dataStorage);
        return machines;
    }
}
