import java.util.List;

//Slide 5
public interface Storage {
    //Slide5 $1
    void add(Machine machine);

    //Slide5 $2
    boolean remove(Machine machine);

    //Slide5 $3
    List<Machine> getAll();



}
