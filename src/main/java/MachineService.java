import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MachineService {

    private MachineRepository repository;

    public MachineService(MachineRepository repository) {
        this.repository = repository;
    }
// slajd 10 $1
    public boolean addIfNotExists(Machine machine){
        if (repository.getAll().contains(machine)) {
            return false;
        } else {
            repository.add(machine);
            return true;
        }

    }

//Slajd 10 $2
    public boolean deleteByName(String machineName) {
        boolean delete = false;
        List<Machine> machines = repository.findByName(machineName);
        for (Machine machine : machines) {
            repository.remove(machine);
            delete = true;
        }
        return delete;
    }

//Slajd 10 $3
    public boolean deleteByType(MachineType machineType) {
        boolean delete = false;
       List<Machine> machines = repository.findByType(machineType);
       for (Machine machine : machines) {
           repository.remove(machine);
           delete = true;
       }
       return delete;
    }
//Slajd 10 $4
    public List<Machine> findWithName(String machineName) {
        return repository.getAll().stream()
                .filter(machine -> machine.getName() == machineName)
                .collect(Collectors.toList());
    }
//Slajd 10 $5
    public List<Machine> findWithType(MachineType machineType) {
        return repository.getAll().stream()
                .filter(machine -> machine.getType() == machineType)
                .collect(Collectors.toList());
    }

//Slajd 10 $6
    public List<Machine> findAll() {
        return repository.getAll();
    }


}
